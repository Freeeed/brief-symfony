<?php

namespace App\Controller;

use DateTime;
use App\Entity\Game;
use App\Entity\User;
use App\Form\GameType;
use DateTimeImmutable;
use App\Entity\Comment;
use App\Form\CommentType;
use App\Repository\GameRepository;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[Route('/game')]
class GameController extends AbstractController
{
    #[IsGranted('ROLE_ADMIN')]
    #[Route('/', name: 'game_index', methods: ['GET'])]
    public function index(GameRepository $gameRepository): Response
    {
        return $this->render('game/index.html.twig', [
            'games' => $gameRepository->findAll(),
        ]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/new', name: 'game_new', methods: ['GET','POST'])]
    public function new(Request $request, SluggerInterface $slugger): Response
    {
        $game = new Game();
        $game->setCreateAt(new DateTimeImmutable('now'));
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

             /** @var UploadedFile $imageType */
             $imageGame = $form->get('game_thumbnail')->getData();
 
             // this condition is needed because the 'imageType' field is not required
             // so the PDF/JPG/PNG file must be processed only when a file is uploaded
             if ($imageGame) {
                 $originalFilename = pathinfo($imageGame->getClientOriginalName(), PATHINFO_FILENAME);
                 // this is needed to safely include the file name as part of the URL
                 $safeFilename = $slugger->slug($originalFilename);
                 $newFilename = $safeFilename.'-'.uniqid().'.'.$imageGame->guessExtension();
  
                 // Move the file to the directory where images are stored
                 try {
                     $imageGame->move(
                         $this->getParameter('images_directory_game'),
                         $newFilename
                     );
                 } catch (FileException $e) {
                     // ... handle exception if something happens during file upload
                 }
  
                 // updates the 'imageTypename' property to store the PDF/JPG/PNG file name
                 // instead of its contents
                 $game->setGameThumbnail($newFilename);
             }
             

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($game);
            $entityManager->flush();

            return $this->redirectToRoute('game_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('game/new.html.twig', [
            'game' => $game,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'game_show', methods: ['GET','POST'])]
    public function show(Game $game, Request $request): Response
    {   
        $user = $this->getUser();
        $comment = new Comment();
        $gameId = $game->getId();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setCommentCreateAt(new \DateTimeImmutable());
            $comment->setCommentGame($game);
            $comment->setCommentUser($user);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();

            return $this->redirectToRoute('game_show', ["id" => $gameId], Response::HTTP_SEE_OTHER);
        }


        return $this->render('game/show.html.twig', [
            'game' => $game,
            'commentForm' => $form->createView(),
        ]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/{id}/edit', name: 'game_edit', methods: ['GET','POST'])]
    public function edit(Request $request, Game $game, SluggerInterface $slugger): Response
    {
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $imageType */
            $imageGame = $form->get('game_thumbnail')->getData();
 
            // this condition is needed because the 'imageType' field is not required
            // so the PDF/JPG/PNG file must be processed only when a file is uploaded
            if ($imageGame) {
                $originalFilename = pathinfo($imageGame->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$imageGame->guessExtension();
 
                // Move the file to the directory where images are stored
                try {
                    $imageGame->move(
                        $this->getParameter('images_directory_game'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
 
                // updates the 'imageTypename' property to store the PDF/JPG/PNG file name
                // instead of its contents
                $game->setGameThumbnail($newFilename);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('game_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('game/edit.html.twig', [
            'game' => $game,
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/{id}', name: 'game_delete', methods: ['POST'])]
    public function delete(Request $request, Game $game): Response
    {
        if ($this->isCsrfTokenValid('delete'.$game->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($game);
            $entityManager->flush();
        }

        return $this->redirectToRoute('game_index', [], Response::HTTP_SEE_OTHER);
    }
}
