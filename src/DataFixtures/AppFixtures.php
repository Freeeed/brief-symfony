<?php

namespace App\DataFixtures;

use App\Entity\Type;
use App\Entity\User;
use App\Entity\Game;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Entity\Comment;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    { 
        $date= new \DateTimeImmutable('NOW');

        // create 5 Types Bam!
        for ($i = 0; $i < 5; $i++) {
            $type = new Type();
            $type->setTypeTitle('type'.$i);
            $type->setTypeDescription('description' .$i);
            $type->setTypeThumbnail('thumnbail' .$i);
            $manager->persist($type);
        }
    
        // create 5 games Bam!

       
        for ($i = 0; $i < 5; $i++) {
            $game = new Game();
            $game->setgameTitle('game '.$i);
            $game->setgameUrl('url '.$i);
            $game->setgameDescription('description' .$i);
            $game->setgameThumbnail('thumnbail' .$i);
            $game->setCreateAt($date);
            $manager->persist($game);
        }
             // create 5 Types Bam!
        for ($i = 0; $i < 5; $i++) {
            $user = new User();
            $user->setNickname('surnom '.$i);
            $user->setEmail('email' .$i);
            $user->setPassword('pasword' .$i);
            $user->setRoles(["ROLE_USER"]);
            $manager->persist($user);
        }

        {
        $user = new User();
        $user->setNickname('Admin');
        $user->setPassword('$2y$13$e940x/lzJg5Sa7r/I9FGxeB3EB7aNEM9PUJXPXcr5G7Cf53UTq5gi');
        $user->setEmail('admin@gmail.com');
        $user->setRoles(["ROLE_ADMIN"]);
        $manager->persist($user);
        }

      // create 5 Comment Bam!
      for ($i = 0; $i < 5; $i++) {
        $comment = new Comment();
        $comment->setCommentContent('jeu nul - jeu extrordinaire '.$i);
        $comment->setCommentRate(mt_rand(0,6));
        $comment->setCommentCreateAt($date);
        $comment->setCommentGame($game);
        $comment->setCommentUser($user);
        $comment->setValid(true);
        $manager->persist($comment);
    }
        $manager->flush();
    }
        
    }

    
    
    