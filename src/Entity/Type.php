<?php

namespace App\Entity;

use App\Repository\TypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeRepository::class)
 */
class Type
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type_title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $type_description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type_thumbnail;

    /**
     * @ORM\ManyToMany(targetEntity=Game::class, mappedBy="game_type")
     */
    private $games;

    public function __construct()
    {
        $this->games = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeTitle(): ?string
    {
        return $this->type_title;
    }

    public function setTypeTitle(string $type_title): self
    {
        $this->type_title = $type_title;

        return $this;
    }

    public function getTypeDescription(): ?string
    {
        return $this->type_description;
    }

    public function setTypeDescription(?string $type_description): self
    {
        $this->type_description = $type_description;

        return $this;
    }

    public function getTypeThumbnail(): ?string
    {
        return $this->type_thumbnail;
    }

    public function setTypeThumbnail(?string $type_thumbnail): self
    {
        $this->type_thumbnail = $type_thumbnail;

        return $this;
    }

    /**
     * @return Collection|Game[]
     */
    public function getGames(): Collection
    {
        return $this->games;
    }

    public function addGame(Game $game): self
    {
        if (!$this->games->contains($game)) {
            $this->games[] = $game;
            $game->addGameType($this);
        }

        return $this;
    }

    public function removeGame(Game $game): self
    {
        if ($this->games->removeElement($game)) {
            $game->removeGameType($this);
        }

        return $this;
    }
}
