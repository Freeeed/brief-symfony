<?php

namespace App\Entity;

use App\Repository\CommentRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommentRepository::class)
 */
class Comment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $comment_content;

    /**
     * @ORM\Column(type="integer")
     */
    private $comment_rate;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $comment_createAt;

    /**
     * @ORM\ManyToOne(targetEntity=Game::class, inversedBy="comments")
     */
    private $comment_game;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="comments")
     */
    private $comment_user;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCommentContent(): ?string
    {
        return $this->comment_content;
    }

    public function setCommentContent(string $comment_content): self
    {
        $this->comment_content = $comment_content;

        return $this;
    }

    public function getCommentRate(): ?int
    {
        return $this->comment_rate;
    }

    public function setCommentRate(int $comment_rate): self
    {
        $this->comment_rate = $comment_rate;

        return $this;
    }

    public function getCommentCreateAt(): ?\DateTimeImmutable
    {
        return $this->comment_createAt;
    }

    public function setCommentCreateAt(\DateTimeImmutable $comment_createAt): self
    {
        $this->comment_createAt = $comment_createAt;

        return $this;
    }

    public function __construct() {
        $this->comment_createAt = new \DateTimeImmutable();
    }

    public function getCommentGame(): ?Game
    {
        return $this->comment_game;
    }

    public function setCommentGame(?Game $comment_game): self
    {
        $this->comment_game = $comment_game;

        return $this;
    }

    public function getCommentUser(): ?User
    {
        return $this->comment_user;
    }

    public function setCommentUser(?User $comment_user): self
    {
        $this->comment_user = $comment_user;

        return $this;
    }

}
