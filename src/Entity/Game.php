<?php

namespace App\Entity;

use App\Repository\GameRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GameRepository::class)
 */
class Game
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $game_title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $game_url;

    /**
     * @ORM\Column(type="text")
     */
    private $game_description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $game_thumbnail;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $game_rating;

    /**
     * @ORM\ManyToMany(targetEntity=Type::class, inversedBy="games")
     */
    private $game_type;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="comment_game")
     */
    private $comments;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="game_user")
     */
    private $users;

    public function __construct()
    {
        $this->game_type = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGameTitle(): ?string
    {
        return $this->game_title;
    }

    public function setGameTitle(string $game_title): self
    {
        $this->game_title = $game_title;

        return $this;
    }

    public function getGameUrl(): ?string
    {
        return $this->game_url;
    }

    public function setGameUrl(string $game_url): self
    {
        $this->game_url = $game_url;

        return $this;
    }

    public function getGameDescription(): ?string
    {
        return $this->game_description;
    }

    public function setGameDescription(string $game_description): self
    {
        $this->game_description = $game_description;

        return $this;
    }

    public function getGameThumbnail(): ?string
    {
        return $this->game_thumbnail;
    }

    public function setGameThumbnail(?string $game_thumbnail): self
    {
        $this->game_thumbnail = $game_thumbnail;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeImmutable
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeImmutable $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getGameRating(): ?string
    {
        return $this->game_rating;
    }

    public function setGameRating(?string $game_rating): self
    {
        $this->game_rating = $game_rating;

        return $this;
    }

    /**
     * @return Collection|Type[]
     */
    public function getGameType(): Collection
    {
        return $this->game_type;
    }

    public function addGameType(Type $gameType): self
    {
        if (!$this->game_type->contains($gameType)) {
            $this->game_type[] = $gameType;
        }

        return $this;
    }

    public function removeGameType(Type $gameType): self
    {
        $this->game_type->removeElement($gameType);

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setCommentGame($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getCommentGame() === $this) {
                $comment->setCommentGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addGameUser($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeGameUser($this);
        }

        return $this;
    }
}
