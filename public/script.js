let constrain = 400;
let mouseOverContainer = document.getElementById("ex1");
let ex1Layer = document.getElementById("ex1-layer");
let lastRotation = {
    x: 0,
    y: 0
}
const snapbackSpeed = 0.01;

function transforms(x, y, el) {
  let box = el.getBoundingClientRect();
  let calcX = -(y - box.y - (box.height / 2)) / constrain;
  let calcY = (x - box.x - (box.width / 2)) / constrain;
  lastRotation.x = calcX;
  lastRotation.y = calcY;
  
  return "perspective(100px) "
    + "   rotateX("+ calcX +"deg) "
    + "   rotateY("+ calcY +"deg) ";
};

 function transformElement(el, xyEl) {
  el.style.transform  = transforms.apply(null, xyEl);
}

mouseOverContainer.onmousemove = function(e) {
  let xy = [e.clientX, e.clientY];
  let position = xy.concat([ex1Layer]);

  window.requestAnimationFrame(function(){
    transformElement(ex1Layer, position);
  });
};

mouseOverContainer.addEventListener('mouseout', function(event){
    console.log('mouseout');
    setTimeout(function() {
        snapBackSmooth(event.target)
        //event.target.style.transform='none';
        console.log('mouseout');
    }, 100)
   
});

function snapBackSmooth(el) {
    window.requestAnimationFrame(function(){
        lastRotation.x = setRotationTick(lastRotation.x)
        lastRotation.y = setRotationTick(lastRotation.y)
        el.style.transform = perspective(lastRotation.x, lastRotation.y);
        if(lastRotation.x !== 0 || lastRotation.y !== 0) snapBackSmooth(el);
    });
}

function setRotationTick(rotation) {
    let newRotation = rotation > 0 ? rotation - snapbackSpeed : rotation + snapbackSpeed;
    console.log((Math.abs(newRotation)));
    if(Math.abs(newRotation) <= snapbackSpeed ) {
        return 0
    }

    return newRotation;
}

function perspective(calcX, calcY) {
    return "perspective(100px) "
    + "   rotateX("+ calcX +"deg) "
    + "   rotateY("+ calcY +"deg) ";
}

//////////////////////////////////////////////////////////////////////////////////////